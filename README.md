# A Simple Card Component


## Overview
For the purpose of this assignment this React project was setup with "create-react-app" (ejected) and uses React version 16.4.2. The basic setup was kept and further in depth optimization has been skipped due to the mere purpose of displaying general features.

### Project structure
The project is build with an Atomic Design pattern which complements the component-based structure of React. Due to this choice the React components as well as the SASS files have been structured in the following way:

    - atoms (smallest units)
    - molecules (composites of atoms)
    - organisms (composites of molecules and/or atoms)

To accommodate this choice the CSS/SASS styles aren't being stored and loaded with its represented component but setup as one "external" stylesheet "main.scss" / "main.css". Class names are not being imported from the SASS.

The overall styling has been kept to a minimum to better show the functionality of the component(s).

The project has 2 pages. 

    - Homepage/Styleguide: "/"
    - Examples: "/examples"

The "Homepage" that explains the build and usage of the components Card and CardList and "Examples" which shows examples of the component Card in a grid (by usage of component CardList)


## Setup
Install project dependencies:

    npm install
    
### Development
To start local development environment run (from root)

    npm run start

### Production
To create a production build run

    npm run build

### Static export
The production build of the project can be found in the folder /build (after running "npm run build") and its contents can be viewed locally with a static server.
Eg: If http-server is globally installed (npm install http-server -g) run the following from the project root:

    http-server ./build

To view the site, open the browser at

    localhost:8080
