import React from 'react';

const Picture = (props) => {

    let sources = [];
    let picture = null;

    const medias = {
        'xs': '(max-width: 499px)',
        's': '(min-width: 500px) AND (max-width: 767px)',
        'm': '(min-width: 768px) AND (max-width: 1023px)',
        'l': '(min-width: 1024px) AND (max-width: 1199px)',
        'xl': '(min-width: 1200px)',
    }

    for (let key in props.sources) {
        let source = <source key={key} srcSet={props.sources[key]} media={medias[key]} />
        sources.push(source);
    }

    if (Object.keys(props.sources).length > 0) {
        picture = (
            <picture className="a-picture">
                {sources}
                <img src={props.sources[Object.keys(props.sources)[0]]} className="a-picture__image" alt={props.alt} />
            </picture>
        )
    }

    return (
        picture
    );
}

export default Picture;