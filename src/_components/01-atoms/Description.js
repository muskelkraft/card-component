import React from 'react';

const Description = (props) => {

    let description = props.children ? <div className="a-text">{props.children}</div> : <p className="a-text">{props.text}</p>;

    return (
        description
    );
}

export default Description;