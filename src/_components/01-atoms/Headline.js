import React from 'react';

const Headline = (props) => {

    const validTags = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
    let headlineClasses = props.classes ? 'a-headline ' + props.classes.split(',').join(' ') : 'a-headline';
    let headline = null;

    if (props.title) {
        if (props.tag && validTags.includes(props.tag.toString())) {
            if (props.children) {
                headline = (
                    <props.tag className={headlineClasses}>
                        {props.children}
                    </props.tag>
                )
            } else {
                headline = <props.tag className={headlineClasses}>{props.title}</props.tag>
            }
        } else {
            if (props.children) {
                headline = (
                    <h3 className={headlineClasses}>
                        {props.children}
                    </h3>
                )
            } else {
                headline = <h3 className="a-headline">{props.title}</h3>
            }
        }
    }
    
    return (
        headline
    );
}

export default Headline;