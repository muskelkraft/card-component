import React from 'react';

const Tag = (props) => {

    let tag = null;

    if (props.label) {
        if(props.link) {
            tag = (
                <a href={props.link} className="a-tag">
                    <span>{props.label}</span>
                </a>
            )
        } else {
            tag = <span>{props.label}</span>
        }
    }

    return (
        tag
    );
}

export default Tag;