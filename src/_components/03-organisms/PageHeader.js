import React from 'react';

const PageHeader = (props) => {

    return (
        <header className="o-app-header" role="banner">
            <div className="container">
                <h1 className="a-headline">{props.title}</h1>
            </div>
        </header>
    );
}

export default PageHeader;