import React from 'react';

import Card from '../02-molecules/Card';
import Headline from '../01-atoms/Headline';


const CardList = (props) => {

    let positionClass = props.position ? 'o-card-list__wrapper--' + props.position : null;
    let headline = props.headline ? <Headline tag="h2" title={props.headline} /> : null;
    let classes = props.listClasses ? props.listClasses.join(' ') : null;

    let cards = props.cards.map((card, index) => (
        <Card 
            key={index}
            cardStyle={props.type}
            gridClasses={card.gridClasses ? card.gridClasses : props.grid}
            headlineTag={card.headlineTag}
            link={card.link}
            title={card.title}
            imageRatio={card.imageRatio ? card.imageRatio : props.imageRatio}
            imageSources={card.imageSources}
            imageAlt={card.imageAlt}
            description={card.description}
            cardTags={card.cardTags}
            cardClasses={card.cardClasses}
        />
    ));

    return (
        <section className="o-card-list cf">
            {headline}
            <div className={'o-card-list__wrapper ' + positionClass + ' ' + classes}>
                {cards}
            </div>
        </section>
    );
}

export default CardList;