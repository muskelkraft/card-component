import React from 'react';

const PageFooter = (props) => {

    return (
        <footer className="o-app-footer" role="contentinfo">
            <div className="container">
                <span>by Lene Gadewoll, 2018</span>
            </div>
        </footer>
    );
}

export default PageFooter;