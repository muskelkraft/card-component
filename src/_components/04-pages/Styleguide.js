import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import Card from '../02-molecules/Card';
import Headline from '../01-atoms/Headline';
import Description from '../01-atoms/Description';

const Styleguide = (props) => {

    return (
        <Fragment>
            <section>
                <Headline tag="h2" title="The assignment" />
                <Description>
                    <p>Create a simple, stateless <b>Card</b> component with two layout variants <i>(vertical, horizontal)</i> in React.<br/>
                    The component can hold the following low-level components: </p>
                    <ul>
                        <li>Picture</li>
                        <li>Headline</li>
                        <li>Description</li>
                        <li>Tag</li>
                    </ul>
                </Description>
            </section>
                
            <section>
                <Headline tag="h2" title="The component" />
                <Headline tag="h3" title="Options" />
                <Description>
                    <p>The <b>Card</b> component has following options:</p>
                    <ul>
                        <li>
                            cardStyle
                            <ul>
                                <li>vertical (default) | horizontal</li>
                            </ul>
                        </li>
                        <li>
                            gridClasses
                            <ul>
                                <li>sets width classes (12-grid, ex: "col-xs-12") for different breakpoints (xs, s, m, l)</li>
                                <li>default: 100%</li>
                            </ul>
                        </li>
                        <li>
                            headlineTag
                            <ul>
                                <li>technically, not necessarily semantically valid tags: h1, h2, h3, h4, h5, h6</li>
                                <li>default: h3</li>
                            </ul>
                        </li>
                        <li>
                            link (optional)
                        </li>
                        <li>
                            title (optional)
                        </li>
                        <li>
                            imageRatio
                            <ul>
                                <li>variations: 16-9, 4-3, 2-1, 1-1</li>
                                <li>default: 16-9</li>
                                <li>on cardStyle="horizontal" pictures have 100% height and auto width</li>
                            </ul>
                        </li>
                        <li>
                            imageSources
                            <ul>
                                <li>object containing picture sources</li>
                                <li>keys for the breakpoints (xs, s, m ,l) are obligatory but not all are mandatory</li>
                            </ul>
                        </li>
                        <li>
                            imageAlt
                            <ul>
                                <li>sets image alt text</li>
                            </ul>
                        </li>
                        <li>
                            description (optional)
                        </li>
                        <li>
                            cardTags
                            <ul>
                                <li>list containing tag objects</li>
                                <li>keys: label (required for tag to be displayed), link (optional)</li>
                            </ul>
                        </li>
                        <li>
                            cardClasses
                            <ul>
                                <li>list of optional classes to be added to <b>Card</b> component</li>
                            </ul>
                        </li>
                    </ul>
                </Description>
            </section>
            <section>
                <Headline tag="h2" title="Usage examples // single card" />
                <Headline tag="h3" title="vertical" />

                <div className="o-card-list cf">
                    <div className="o-card-list__wrapper no-stretch">
                        <Card 
                            cardStyle="vertical"
                            gridClasses="col-xs-12 col-s-6 col-m-5 col-l-5"
                            headlineTag="h3"
                            link="/somewhere"
                            title="Card Title"
                            imageRatio="16-9"
                            imageSources={{xs: "http://satyr.io/500x16:9/1", s: "http://satyr.io/767x16:9/2", m: "http://satyr.io/1024x16:9/3", l: "http://satyr.io/1200x16:9/4", xl: "http://satyr.io/1440x16:9/5"}}
                            imageAlt="image placeholder"
                            description="Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                            cardTags={[{label: 'Tag 1', link: '#tag1'}]}
                            cardClasses={[]}
                        />
                        <div className="o-code-block col-xs-12 col-s-6 col-m-7 col-l-7 float-l">
                            <code className="a-code">
                                {'<Card'} <br />
                                    {'cardStyle="vertical"'} <br />
                                    {'gridClasses="col-xs-12 col-s-6 col-m-5 col-l-5"'} <br />
                                    {'headlineTag="h3"'} <br />
                                    {'link="/somewhere"'} <br />
                                    {'title="Card Title"'} <br />
                                    {'imageRatio="16-9"'} <br />
                                    {'imageSources={{'} <br />
                                        {'xs: "http://satyr.io/500x16:9/1",'} <br />
                                        {'s: "http://satyr.io/767x16:9/2",'} <br />
                                        {'m: "http://satyr.io/1024x16:9/3",'} <br /> 
                                        {'l: "http://satyr.io/1200x16:9/4", '} <br />
                                        {'xl: "http://satyr.io/1440x16:9/5"}'} <br />
                                    {'}}'} <br />
                                    {'imageAlt="image placeholder"'} <br />
                                    {'description="Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit."'} <br />
                                    {'cardTags={[{label: "Tag 1", link: "#tag1"}]}'} <br />
                                    {'cardClasses={[]}'} <br />
                                {'/>'}
                            </code>
                        </div>
                    </div>

                    <Headline tag="h3" title="horizontal" />

                    <div className="o-card-list__wrapper no-stretch">
                        <Card 
                            cardStyle="horizontal"
                            gridClasses="col-xs-12 col-s-12 col-m-7 col-l-7"
                            headlineTag=""
                            link="/somewhereelse"
                            title="Card Title"
                            imageRatio="16-9"
                            imageSources={{xs: "http://satyr.io/500x16:9/1", s: "http://satyr.io/767x16:9/2", m: "http://satyr.io/1024x16:9/3", l: "http://satyr.io/1200x16:9/4", xl: "http://satyr.io/1440x16:9/5"}}
                            imageAlt="image placeholder"
                            description="Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                            cardTags={[{label: 'Tag 1', link: '#tag1'}]}
                            cardClasses={[]}
                        />

                        <div className="o-code-block col-xs-12 col-s-12 col-m-5 col-l-5 float-l">
                            <code className="a-code">
                                {'<Card'} <br />
                                    {'cardStyle="horizontal"'} <br />
                                    {'gridClasses="col-xs-12 col-s-12 col-m-7 col-l-7"'} <br />
                                    {'headlineTag="h3"'} <br />
                                    {'link="/somewhereelse"'} <br />
                                    {'title="Card Title"'} <br />
                                    {'imageRatio="16-9"'} <br />
                                    {'imageSources={{'} <br />
                                        {'xs: "http://satyr.io/500x16:9/1",'} <br />
                                        {'s: "http://satyr.io/767x16:9/2",'} <br />
                                        {'m: "http://satyr.io/1024x16:9/3",'} <br /> 
                                        {'l: "http://satyr.io/1200x16:9/4", '} <br />
                                        {'xl: "http://satyr.io/1440x16:9/5"}'} <br />
                                    {'}}'} <br />
                                    {'imageAlt="image placeholder"'} <br />
                                    {'description="Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit."'} <br />
                                    {'cardTags={[{label: "Tag 1", link: "#tag1"}]}'} <br />
                                    {'cardClasses={[]}'} <br />
                                {'/>'}
                            </code>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                    <Headline tag="h2" title="Usage examples // card grid" />
                    <Description>
                        The <b>Card</b> component can be used as single component or as part of a <b>CardList</b>.
                        While being used in <b>CardList</b> the options <i>gridClasses</i> and <i>imageRatio</i> can 
                        either be set generally for all children by setting them on the list, or can still 
                        be set for each child seperately.
                    </Description>

                    <Headline tag="h3" title="Options" />

                    <Description>
                        <p>The <b>CardList</b> component has following options:</p>
                        <ul>
                            <li>
                                style
                                <ul>
                                    <li>vertical (default) | horizontal</li>
                                </ul>
                            </li>
                            <li>
                                headline (optional)
                                <ul>
                                    <li>fix tag: h2</li>
                                </ul>
                            </li>
                            <li>
                                position
                                <ul>
                                    <li>positions alignment of children</li>
                                    <li>values: start (default), center, end</li>
                                </ul>
                            </li>
                            <li>
                                grid
                                <ul>
                                    <li>sets width classes (12-grid) for different breakpoints (xs, s, m, l) for <u>all</u> children</li>
                                    <li>default: 100%</li>
                                </ul>
                            </li>
                            <li>
                                listClasses (optional)
                                <ul>
                                    <li>list of optional classes to be added to grid wrapper container</li>
                                </ul>
                            </li>
                            <li>
                                cards
                                <ul>
                                    <li>list of card objects that will be rendered as children</li>
                                </ul>
                            </li>
                        </ul>
                    </Description>

                    <Description>
                        By default the child components will stretch to fit each others height. To disable this behavior, 
                        the class <i>no-stretch</i> can be added in the <i>listClasses</i> array.
                    </Description>

                    <Headline tag="h3" title="Data structure example" />

                    <Description>
                        In the following example the options for <i>grid</i> and <i>imageRatio</i> set in <b>CardList</b> will 
                        be overriden on the child. The child will inherit the parents options as soon as its own options are not set.
                    </Description>

                    <code className="a-code">
                    {'"cardLists": [{'}<br />
                        {'"style": "vertical",'}<br />
                        {'"headline": "Card List // vertical - equal"'}<br />
                        {'"position": "start",'}<br />
                        {'"grid": "col-xs-12 col-s-6 col-m-6 col-l-3",'}<br />
                        {'"imageRatio": "16-9",'}<br />
                        {'"cards": [{'}<br />
                            {'"headlineTag": "h3",'}<br />
                            {'"gridClasses": "col-xs-12 col-s-6 col-m-4 col-l-4",'}<br />
                            {'"link": "#",'}<br />
                            {'"title": "Card Title 1",'}<br />
                            {'"imageAlt": "Amazing Image",'}<br />
                            {'"imageSources": {'}<br />
                                {'"xs": "http://satyr.io/500x16:9/1",'}<br />
                                {'"s": "http://satyr.io/767x16:9/2",'}<br />
                                {'"m": "http://satyr.io/1024x16:9/3",'}<br />
                                {'"l": "http://satyr.io/1200x16:9/4",'}<br />
                                {'"xl": "http://satyr.io/1440x16:9/5"'}<br />
                            {'},'}<br />
                            {'"imageRatio": "4-3",'}<br />
                            {'"description": "Curabitur aliquet quam id dui posuere blandit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin eget tortor risus.",'}<br />
                            {'"cardTags": [{'}<br />
                                {'"label": "tag 1",'}<br />
                                {'"link": "#tag1"'}<br />
                            {'}, {'}<br />
                                {'"label": "tag 2",'}<br />
                                {'"link": "#tag2"'}<br />
                            {'}],'}<br />
                            {'"cardClasses": []'}<br />
                        {'}]'}<br />
                    </code>

                    <Headline tag="h3" title="Grid examples" />

                    View <Link className="a-underlined" to="/examples">examples</Link> of a <b>CardList</b> grid.
            </section>

        </Fragment>
    );
}

export default Styleguide;