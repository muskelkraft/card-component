import React from 'react';
import { Link } from 'react-router-dom';

import Picture from '../01-atoms/Picture';
import Headline from '../01-atoms/Headline';
import Description from '../01-atoms/Description';
import Tag from '../01-atoms/Tag';


const Card = (props) => {

    let cardClass = 'm-card';
    let tagsArray = [];
    let tags = null;
    let headline = null;
    let image = null;
    let imageRatio = props.imageRatio ?  'aspect-' + props.imageRatio  : null;
    let classes = null;
    let styleClass = null;
    let gridClasses = props.gridClasses;

    if (props.cardStyle) {
        styleClass = cardClass + '--' + props.cardStyle;
    }

    classes = cardClass + ' ' + gridClasses + ' float-l ' + (props.cardClasses ? props.cardClasses.join(' ') : '') + ' ' + styleClass;

    if(Object.keys(props.cardTags).length > 0) {
        for (let key in props.cardTags) {
            let item = props.cardTags[key];
            let tag = <Tag key={key} label={item.label} link={item.link} />
            tagsArray.push(tag);
        }

        tags = (
            <footer className="m-card__tag-list">
                {tagsArray}
            </footer>
        )
    } else {
        tagsArray = null;
    }

    if (Object.keys(props.imageSources).length > 0) {

        if (props.link) {
            image = (
                <div className="m-card__image-wrapper">
                    <Link className="l-card" to={{
                        pathname: props.link
                    }}>
                        <div className={imageRatio}>
                            <Picture sources={props.imageSources} alt={props.imageAlt} />
                        </div>
                    </Link>
                </div>
            )
        } else {
            image = (
                <div className="m-card__image-wrapper">
                    <div className={imageRatio}>
                        <Picture sources={props.imageSources} alt={props.imageAlt} />
                    </div>
                </div>
            )
        }
    }

    if (props.title && props.link) {
        headline = (
            <Headline tag={props.headlineTag} title={props.title}>
                <Link className="l-card" to={{
                    pathname: props.link
                }}>
                    <span>{props.title}</span>
                </Link>
            </Headline>
        )
    } else {
        headline = <Headline tag={props.headlineTag} title={props.title} />
    }

    return (
        <article className={classes}>
            <div className="m-card__wrapper">
                {image}
                <div className="m-card__content-wrapper">
                    {headline}
                    <Description text={props.description} />
                    {tags}
                </div>
            </div>
        </article>
    );
}

export default Card;