import React, { Component, Fragment } from 'react';
import { Route, Switch, Redirect, Link } from 'react-router-dom';

import PageHeader from '../_components/03-organisms/PageHeader';
import PageFooter from '../_components/03-organisms/PageFooter';
import CardList from '../_components/03-organisms/CardList';
import Styleguide from '../_components/04-pages/Styleguide';

import data from '../_data/data.json';


class App extends Component {

    state = data;

    componentDidUpdate() {
        window.scrollTo(0, 0);
    }

    render() {

        let cardLists = this.state.cardLists.map((list, index) => (
            <CardList key={index} type={list.style} headline={list.headline} position={list.position} grid={list.grid} imageRatio={list.imageRatio} listClasses={list.listClasses} cards={list.cards} />
        ));

        return (
            <Fragment>
                <div className="app-page col-xs-12 col-s-12 col-m-10 col-l-10">

                <Switch>
                    <Route path="/" exact render={() => (
                        <Fragment>
                            <PageHeader title="A Simple Card Component" />
                                <div className="app-content container">
                                    <main className="app-main" role="main">
                                        <Styleguide />
                                    </main>
                                </div>
                            <PageFooter />
                        </Fragment>
                    )} />
                    <Route path="/examples" exact render={() => (
                        <Fragment>
                            <PageHeader title="Card Component Examples" />
                                <div className="app-content container">
                                    <main className="o-app-main" role="main">
                                        {cardLists}
                                        <div className="divider-large"></div>
                                        <div className="cf">Back to <Link className="a-underlined" to="/">Styleguide</Link></div>
                                    </main>
                                </div>
                            <PageFooter />
                        </Fragment>
                    )} />
                    <Redirect to="/" />
                    <Route render={() => <PageHeader title="404 page not found" />} />
                </Switch>
                    
                </div>
            </Fragment>
        );
    }

}

export default App;
