import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './scss/main.scss';

import App from './_containers/App';
import registerServiceWorker from './registerServiceWorker';


const app = (
    <BrowserRouter basename="/">
        <App />
    </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
